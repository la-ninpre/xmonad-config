# xmonad config

this is rather simple xmobar config of mine. see xmonad.hs for details.

![preview](doc/preview.jpg)

## dependencies

this config will work only for xmonad >=0.17!

### libraries

this list can be different depending on distro,
consult your distro's package list for specific package names.

* libx11-dev
* libxinerama-dev (for multiple screens)
* libxext-dev
* libxrandr-dev
* libxss-dev
* libxft-dev
* libxpm-dev

### software

these are not strict dependencies. it's just that keybindings are
set to spawn them. feel free to change any of these to your liking.

* dmenu (run launcher)
* alacritty (terminal emulator)

#### optional

* thunar (file manager)
* firefox (web browser)
* mpv (media player)
* moc (music on console)
* qalculate-gtk (calculator)
* thunderbird (mail client)

### fonts

* lato
* [linja pona][4]

#### optional

* Press Start 2P
* Font Awesome
* Font Awesome Brands

## installation

this guide assumes that xmonad-0.17 is available from your distro's repos.
if it's not the case, the better option is to use stack to build latest xmonad,
xmonad-contrib and xmobar (see example/ for details).

1. install xmonad through your package manager.

2. clone this repository to `~/.config/xmonad`

    ```
    $ git clone https://git.aaoth.xyz/xmonad-config.git ~/.config/xmonad
    ```

3. copy `example/xmonad.desktop` file to `/usr/share/xsessions` if you are using
display manager

    ```
    $ sudo cp xmonad.desktop /usr/share/xsessions
    ```

    otherwise, add this to your `~/.xinitrc`:

    ```
    exec xmonad
    ```

4. log in and enjoy

## keybindings

*work in progress*

[1]:https://gitlab.com/dwt1/dotfiles/-/tree/master/.xmonad
[2]:https://docs.haskellstack.org/en/stable/README/
[3]:https://gitlab.com/dwt1/dmscripts
[4]:http://musilili.net/linja-pona/
