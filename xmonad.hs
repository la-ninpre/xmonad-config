import XMonad

import XMonad.Hooks.EwmhDesktops (ewmhFullscreen, ewmh)
import XMonad.Hooks.StatusBar (defToggleStrutsKey, withEasySB)

import XMonad.Config.LaNinpre
    (LayoutNames(..)
    , laNinpreConfig
    , statusBarConfig
    )

wsMin = [ "<s>", "<m>", "<p>", "<l>", "<o>", "<t>", "<u>", "<i>", "<a>" ]

layoutNames = LayoutNames "l" "s" "u"

configMin = laNinpreConfig wsMin layoutNames

main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB statusBarConfig defToggleStrutsKey
     $ configMin
