if xmonad >=0.17 isn't available from your distro's package manager,
the better option would be to use stack to build xmonad.

install stack using your package manager or consider [stack's documentation][1].

[1]:https://docs.haskellstack.org/en/stable/README/

you need to copy `example/stack.yml.example` to
`~/.config/xmonad/stack.yml`. and to copy `example/build` script to
`~/.config/xmonad/build`. these are needed for xmonad to use stack for
rebuilding.

```
$ cp ~/.config/xmonad/example/stack.yml.example ~/.config/xmonad/stack.yaml
$ cp ~/.config/xmonad/example/build ~/.config/xmonad/build
```

also you need to fetch bunlded submodules using git (or provide xmonad-git and
xmonad-contrib-git using some other way).

```
$ git submodule init
$ git submodule update
```
